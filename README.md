# repbench-results

This repository collects the results of the [`repbench`](https://marcel.science/repbench) project. It is structured as follows:

- `data/` contains the learning curves and timings in `.yml` format
- `models/` contains the tuned models and search spaces, also in `.yml`

Both of these folders have sub-folders with the name of the dataset:

- `qm9`: QM9 (molecule) dataset. Losses are in kcal/mol.
- `ba10`: BA10 (binary alloy) dataset. Losses are in eV/atom.
- `nmd18`: NMD18u (nomad 2018 kaggle challenge, unrelaxed geometries) dataset. Losses in eV/cation.
- `xnmd18`: NMD18r (nomad 2018 kaggle challenge, relaxed geometries) dataset. Losses in eV/cation.

All files can be loaded with any `yaml` reader, or directly via `cmlkit.engine.read_yaml(filename)`.


The curves are structured as follows:
```
model_type:
  n_train:
    duration: (time to evaluate, can be ignored)
    lossname:
      all: [loss for all 10 outer train/test splits]
      mean: mean loss
      std: std of loss
```

The timings:
```
model_type:
  n_train:
    max: maximum time over 3 repeats
    mean: mean time over 3 repeats
    min: minimum time over 3 repeats
    times: [all 3 times]
   ...

```

The models:

Filename is model type.
```
n_train:
	# ... entire model config,
	# can be loaded with cmlkit.from_config()
```

The search spaces:

Filename is model type. Then inside is a `cmlkit.tune` search space definition.

Timings are always in seconds, and *not* scaled by the size of the test set, i.e. the representation timings are the time to compute all representations in the test set, and the kernel times are the time to compute all pairwise kernel matrix entries. In order to arrive at the time for a single representation, you therefore need to divide by `n_test`, and to arrive at single kernel evaluation by `n_test**2`. 

The test set sizes are:
- `qm9`: 10000
- `ba10`: 1000
- `nmd18`: 600
- `xnmd18`: 600
